FROM node:18 as build

WORKDIR /file-service

COPY ./file-service/ /file-service/

RUN yarn add nestjs-minio-client
RUN yarn install
RUN yarn build


EXPOSE 3000

CMD ["npm", "run", "start:prod"]