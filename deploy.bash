docker build -t registry.gitlab.com/finki5765835/serviceorientedarchitecture/fileservice .
docker push registry.gitlab.com/finki5765835/serviceorientedarchitecture/fileservice
export KUBECONFIG=/etc/rancher/k3s/k3s.yaml
helm upgrade -i file file-soa -n file-service --create-namespace